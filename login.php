<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
    <link rel="stylesheet" href="templates/style.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script>
      $( document ).ready(function() {
          console.log( "document loaded" );
          $(".button-collapse").sideNav();
          $('select').material_select();
      });

      $( window ).load(function() {
          console.log( "window loaded" );
      });
    </script>
    <title>Connexion - Application Veille</title>
  </head>
  <body>
    <?php
      include 'templates/navbar.php'
     ?>

      <div class="row">
        <div class="col s12 m6 offset-m3">
          <div class="card">
            <div class="card-content">
              <form class="" action="modules/user_verificator.php" method="post">
                <div class="input-field col s12">
                  <input id="username" name="username" type="text" class="validate">
                  <label for="username">Nom d'utilisateur</label>
                </div>
                <div class="input-field col s12">
                  <input id="passwd" name="passwd" type="password" class="validate">
                  <label for="passwd">Mot de passe</label>
                </div>
                <div class="row center">
                  <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
  </body>
</html>
