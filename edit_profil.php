<?php session_start(); include 'core/connexion.php';?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
  <link rel="stylesheet" href="templates/style.css">
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script>
  $( document ).ready(function() {
    console.log( "document loaded" );
    $(".button-collapse").sideNav();
    $('select').material_select();
  });

  $( window ).load(function() {
    console.log( "window loaded" );
  });
  </script>
  <title>Modification du profil - Application Veille</title>
</head>
<body>

  <?php include 'templates/navbar.php' ?>

  <div class="row">
    <div class="col s12 m6 offset-m3">
      <div class="card">
        <div class="card-content">
          <?php
          $username=$_SESSION['username'];
          $handle=connexion_bdd();
          $query="SELECT * FROM users WHERE username='$username'";
          $result = mysqli_query($handle,$query);
          $line = mysqli_fetch_array($result);
           ?>
          <form action="modules/update_profil.php" method="post">
            <div class="input-field col s12">
              <input id="name" name="name" type="text" class="validate" value="<?php echo $line['name']; ?>" required>
              <label for="name">Nouveau prénom</label>
            </div>
            <div class="input-field col s12">
              <input id="surname" name="surname" type="text" class="validate" value="<?php echo $line['first_name']; ?>" required>
              <label for="surname">Nouveau nom</label>
            </div>
            <div class="input-field col s12">
              <input id="username" name="username" type="text" class="validate" value="<?php echo $line['username']; ?>" required>
              <label for="username">Nouveau nom d'utilisateur</label>
            </div>
            <div class="input-field col s12">
              <input id="passwd" name="passwd" type="password" class="validate" value="<?php echo $line['password']; ?>" required>
              <label for="passwd">Nouveau mot de passe</label>
            </div>
            <div class="input-field col s12">
              <select id="list_promotions" name="list_promotions">
                  <?php
                    $query="SELECT * FROM promotions ORDER by id DESC";
                    $result = mysqli_query($handle,$query);
                    while($line = mysqli_fetch_array($result)) {
                      echo "<option value='" . $line["promotion"] . "'>" . $line["promotion"] . "</option>";
                    }
                  ?>
              </select>
              <label for="list_promotions">Changer de promotion</label>
            </div>
            <div class="row center">
              <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                <i class="material-icons perm_identity">Modifier mon compte utilisateur</i>
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
</body>
</html>
