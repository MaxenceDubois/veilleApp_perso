
  <nav>
    <div class="nav-wrapper">
      <a href="index.php" class="brand-logo">Veille du jour</a>
      <a href="index.php" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <?php
          if (isset($_SESSION['admin']) && $_SESSION['admin']!=NULL) {
            echo "<li><a href='profil.php' class='waves-effect waves-light btn btn-flat white-text'>" . $_SESSION['admin'] . "</a></li>";
            echo "<li><a href='modules/disconnect.php' class='waves-effect waves-light btn btn-flat white-text'>Déconnexion</a></li>";
          } else {
            echo "<li><a href='login_backoffice.php' class='waves-effect waves-light btn btn-flat white-text'>Connexion</a></li>";
          }
         ?>
      </ul>
      <ul class="side-nav" id="mobile-demo">
        <?php
          if (isset($_SESSION['admin']) && $_SESSION['admin']!=NULL) {
            echo "<li><a href='profil.php' class='waves-effect waves-light btn btn-flat'>" . $_SESSION['admin'] . "</a></li>";
            echo "<li><a href='modules/disconnect.php' class='waves-effect waves-light btn btn-flat'>Déconnexion</a></li>";
          } else {
            echo "<li><a href='login_backoffice.php' class='waves-effect waves-light btn btn-flat' style='color: white;'>Connexion</a></li>";
          }
         ?>
      </ul>

    </div>
  </nav>
