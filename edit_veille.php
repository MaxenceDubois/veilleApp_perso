<?php session_start(); include 'core/connexion.php'; ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
    <link rel="stylesheet" href="templates/style.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script>
      $( document ).ready(function() {
          console.log( "document loaded" );
          $(".button-collapse").sideNav();
          $('select').material_select();
      });

      $( window ).load(function() {
          console.log( "window loaded" );
      });
    </script>
    <title>Envoyer une veille - Application Veille</title>
  </head>
  <body>

    <?php
      include 'templates/navbar.php'
     ?>

    <div class="row">
        <div class="col s12 m6 offset-m3">
          <div class="card">
            <div class="card-content">
              <form class="" action="modules/update_veille.php" method="post">
                <div class="input-field col s12">
                  <select id="subject" name="subject">
                    <?php
                      $handle=connexion_bdd();
                      $query="SELECT * FROM subject";
                      $result = mysqli_query($handle,$query);
                      while($line = mysqli_fetch_array($result)) {
                        echo "<option value='" . $line["libelle"] . "'>" . $line["libelle"] . "</option>";
                      }
                    ?>
                  </select>
                  <label for="subject">Thème de la veille</label>
                </div>
                <?php
                $id=$_GET['id_veille'];
                $query="SELECT * FROM veille WHERE id=$id";
                $result = mysqli_query($handle,$query);
                $line = mysqli_fetch_array($result);
                 ?>
                <div class="input-field col s12">
                  <input id="title" name="title" type="text" class="validate" value="<?php echo $line['title'] ?>" required>
                  <label for="title">Titre</label>
                </div>
                <div class="input-field col s12">
                  <textarea id="content" name="content" rows="10" cols="50" class="materialize-textarea" required><?php echo $line['content'] ?></textarea>
                  <label for="content">Description</label>
                </div>
                <div class="input-field col s12">
                  <input id="tag" name="tag" type="text" class="validate" value="<?php echo $line['keyword'] ?>" required>
                  <label for="tag">Un mot clé</label>
                </div>
                <input type="hidden" name="id_veille" value="<?php echo $_GET['id_veille']; ?>">
                <div class="row center">
                  <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
  </body>
</html>
