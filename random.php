<?php include 'core/connexion.php'; session_start(); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Page d'accueil - Application Veille</title>
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
  <link rel="stylesheet" href="templates/style.css">
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script>
  $( document ).ready(function() {
    console.log( "document loaded" );
    $(".button-collapse").sideNav();
    $('select').material_select();
  });

  $( window ).load(function() {
    console.log( "window loaded" );
  });
  </script>
</head>
<body>

  <?php
  include 'templates/navbar.php'
  ?>

  <div class="row">
    <div class="col s12 m2 offset-m5">
      <div class="card blue-grey darken-1">
        <div class="card-content white-text center">
          <?php
          $handle = connexion_bdd();
          $query = "SELECT * FROM users ORDER BY RAND()";
          $result = mysqli_query($handle,$query);
          $line=mysqli_fetch_array($result);
          echo $line['first_name'];
           ?>
        </div>
        <div class="card-action">
          <a href="random.php">Nouveau tirage</a>
        </div>
      </div>
    </div>
  </div>

</body>
</html>
