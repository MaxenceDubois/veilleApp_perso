<?php session_start(); $username=$_SESSION['username']; ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Page d'accueil - Application Veille</title>
  </head>
  <body>
    <p>
      En cliquant sur le bouton ci-dessous, vous supprimerez l'intégralité du contenu
      de votre compte. Vos veilles resteront toutefois accessible aux autres utilisateurs.
      Êtes-vous sûr de vouloir continuer ?
    </p>
    <form class="" action="modules/delete_user_account.php" method="post">
      <button type="submit">Supprimer mon compte</button>
    </form>
  </body>
</html>
