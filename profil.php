<?php include "modules/list_news_user.php"; session_start(); $username=$_SESSION['username']; ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
  <link rel="stylesheet" href="templates/style.css">
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script>
  $( document ).ready(function() {
    console.log( "document loaded" );
    $(".button-collapse").sideNav();
    $('select').material_select();
  });

  $( window ).load(function() {
    console.log( "window loaded" );
  });
  </script>
  <title>Profil - Application Veille</title>
</head>
<body>
  <?php
  include 'templates/navbar.php'
  ?>
  <div class="row">
    <div class="container col s12 m8">
      <h5>Toutes vos veilles postées</h5>
      <?php
      $result=list_news_user();
      while($line = mysqli_fetch_array($result)) {
        ?>
        <div class="row">
          <div class="col s12">
            <div class="card blue-grey darken-1">
              <div class="card-content white-text">
                <span class="card-title"><?php echo $line['title']; ?></span>
                <p><?php echo $line['content']; ?></p>
                <p>Tag : <?php echo $line['keyword']; ?></p>
                <p>Date : <?php echo $line['date']; ?></p>
                <?php
                if ($line['files']!=NULL) {
                  echo "Fichier joint : <a href='uploads/" . $line['files'] . "'>" . $line['files'] . "</a>";
                }
                 ?>
              </div>
              <div class="card-action">
                <a href="edit_veille.php?id_veille=<?php echo $line['id']; ?>">Modifier</a>
                <a href="modules/delete_veille.php?id_veille=<?php echo $line['id']; ?>">Supprimer</a>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <div class="container col s12 m4">
        <h5>Informations sur votre compte</h5>
        <div class="row">
          <div class="col s12">
            <div class="card">
              <div class="card-content">
                <?php
                $handle=connexion_bdd() or die('Erreur de connexion à la base de données');
                $query="SELECT * FROM users WHERE username='$username'";
                $result=mysqli_query($handle,$query);
                $line=mysqli_fetch_array($result);
                ?>
                <p>Prénom : <?php echo $line['first_name']; ?></p>
                <p>Nom : <?php echo mb_strtoupper($line['name']); ?></p>
                <p>Promotion : <?php echo $line['promo']; ?></p>
              </div>
              <div class="card-action">
                <div class="row center">
                  <form class="profile_settings col m12 l6" action="edit_profil.php" method="post">
                    <button type="submit" class="waves-effect waves-light btn">Modifier</button>
                  </form>
                  <form class="profile_settings col m12 l6" action="delete_profil.php" method="post">
                    <button type="submit" class="waves-effect waves-light btn red">Supprimer</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
  </body>
  </html>
