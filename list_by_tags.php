<?php

include 'modules/list_news.php';

 ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
    <link rel="stylesheet" href="templates/style.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script>
      $( document ).ready(function() {
          console.log( "document loaded" );
          $(".button-collapse").sideNav();
          $('select').material_select();
      });

      $( window ).load(function() {
          console.log( "window loaded" );
      });
    </script>
    <title>Page d'accueil - Application Veille</title>
  </head>
  <body>
    <?php
      include 'templates/navbar.php'
     ?>
     <div class="row">
         <div class="row col s12 m8">
           <h5>Toutes les veilles sur les <?php echo $_POST['list_by_subject'] ?></h5>
           <?php
             $subject=$_POST['list_by_subject'];
             $handle=connexion_bdd() or die('Erreur de connexion à la base de données');
             $query="SELECT * FROM veille WHERE subject='$subject'";
             $result=mysqli_query($handle,$query);
             while($line = mysqli_fetch_array($result)) {
           ?>
           <div class="col s12">
             <div class="card blue-grey darken-1">
               <div class="card-content white-text">
                 <span class="card-title"><?php echo $line['title']; ?></span>
                 <p><?php echo $line['content']; ?></p>
                 <p>Tags : <?php echo $line['keyword']; ?></p>
               </div>
               <div class="card-action">
                 <a href="#">Supprimer</a>
               </div>
             </div>
           </div>
           <?php } ?>
         </div>
       <div class="container col s12 m4">
         <form class="" action="list_by_tags.php" method="post">
           <div class="input-field col s12">
             <select id="list_by_tags" name="list_by_subject">
               <?php
                 $query="SELECT * FROM subject";
                 $result = mysqli_query($handle,$query);
                 while($line = mysqli_fetch_array($result)) {
                   echo "<option value='" . $line["libelle"] . "'>" . $line["libelle"] . "</option>";
                 }
               ?>
             </select>
             <label for="list_by_tags">Lister par thèmes</label>
           </div>
           <div class="row center">
             <button class="btn waves-effect waves-light" type="submit" name="action">Filtrer
               <i class="material-icons right">search</i>
             </button>
           </div>
         </form>
       </div>
     </div>

     <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
  </body>
</html>
