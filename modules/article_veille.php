<?php include screen_news.php; ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title></title>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script>
      $( document ).ready(function() {
          console.log( "document loaded" );
          $(".button-collapse").sideNav();
      });

      $( window ).load(function() {
          console.log( "window loaded" );
      });
    </script>
  </head>
  <body>
    <?php
      $result=screen_news();
      while($line = mysqli_fetch_array($result)) {
    ?>

    <div class="container">
      <div class="row">
        <span class="card-title"><?php echo $line['title']; ?></span>
        <p><?php echo $line['content']; ?></p>
        <p>Tags : <?php echo $line['keyword']; ?></p>
      </div>
    </div>

  </body>
</html>
