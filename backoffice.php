<?php include "core/connexion.php"; session_start(); $admin=$_SESSION['admin']; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
  <link rel="stylesheet" href="templates/style.css">
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script>
  $( document ).ready(function() {
    console.log( "document loaded" );
    $(".button-collapse").sideNav();
    $('select').material_select();
  });

  $( window ).load(function() {
    console.log( "window loaded" );
  });
  </script>
  <title>Backoffice - Application Veille</title>
</head>
<body>
  <?php
  include 'templates/navbar_backoffice.php'
  ?>
  <div class="container col s12 m6 offset-m3">
    <h5>Informations sur les comptes</h5>
    <div class="row">
      <div class="col s12">
        <div class="card">
          <div class="card-content">
            <?php
            $handle=connexion_bdd() or die('Erreur de connexion à la base de données');
            $query="SELECT * FROM users";
            $result=mysqli_query($handle,$query);
            ?>
            <table>
              <thead>
                <tr>
                  <th data-field="first_name">Prénom</th>
                  <th data-field="name">Nom</th>
                  <th data-field="username">Nom d'utilisateur</th>
                  <th data-field="account">Compte</th>
                </tr>
              </thead>

              <tbody>
                <?php while ($line=mysqli_fetch_array($result)) { ?>
                  <tr>
                    <td><?php echo $line['first_name']; ?></td>
                    <td><?php echo $line['name']; ?></td>
                    <td><?php echo $line['username']; ?></td>
                    <td><?php echo "<a href='modules/connexion_user.php?user=" . $line['username'] . "'>Voir ses veilles</a>"; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container col s12 m6 offset-m3">
      <h5>Créer un compte administrateur</h5>
      <div class="row">
        <div class="col s12">
          <div class="card">
            <div class="card-content">
              <form action="modules/admin_registration.php" method="post">
                <div class="input-field col s12">
                  <input id="name" name="name" type="text" class="validate" required>
                  <label for="name">Entrez votre nom</label>
                </div>
                <div class="input-field col s12">
                  <input id="surname" name="surname" type="text" class="validate" required>
                  <label for="surname">Entrez votre prénom</label>
                </div>
                <div class="input-field col s12">
                  <input id="username" name="username" type="text" class="validate" required>
                  <label for="username">Entrez votre nom d'utilisateur</label>
                </div>
                <div class="input-field col s12">
                  <input id="passwd" name="passwd" type="password" class="validate" required>
                  <label for="passwd">Entrez votre mot de passe</label>
                </div>
                <div class="row center">
                  <button class="btn waves-effect waves-light" type="submit" name="action">Créer le compte administrateur
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container col s12 m6 offset-m3">
      <h5>Modifier mon compte</h5>
      <div class="row">
        <div class="col s12">
          <div class="card">
            <div class="card-content">
              <?php
              $query="SELECT * FROM admin WHERE admin='$admin'";
              $result = mysqli_query($handle,$query);
              $line = mysqli_fetch_array($result);
              ?>
              <form action="modules/update_admin.php" method="post">
                <div class="input-field col s12">
                  <input id="name" name="name" type="text" class="validate" value="<?php echo $line['name']; ?>" required>
                  <label for="name">Nouveau prénom</label>
                </div>
                <div class="input-field col s12">
                  <input id="surname" name="surname" type="text" class="validate" value="<?php echo $line['first_name']; ?>" required>
                  <label for="surname">Nouveau nom</label>
                </div>
                <div class="input-field col s12">
                  <input id="username" name="username" type="text" class="validate" value="<?php echo $line['admin']; ?>" required>
                  <label for="username">Nouveau nom d'utilisateur</label>
                </div>
                <div class="input-field col s12">
                  <input id="passwd" name="passwd" type="password" class="validate" value="<?php echo $line['password']; ?>" required>
                  <label for="passwd">Nouveau mot de passe</label>
                </div>
                <div class="row center">
                  <button class="btn waves-effect waves-light" type="submit" name="action">Modifier mon compte
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container col s12 m6 offset-m3">
      <div class="row">
        <div class="col s12">
          <div class="card">
            <div class="card-content">
              <div class="row center">
                <form class="profile_settings col m12 l6" action="delete_admin.php" method="post">
                  <button type="submit" class="waves-effect waves-light btn red">Supprimer mon compte</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
  </body>
  </html>
