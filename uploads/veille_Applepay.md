http://www.bloomberg.com/news/articles/2016-08-25/apple-said-to-plan-iphone-for-japan-with-tap-to-pay-for-subways
http://www.theverge.com/2016/8/25/12653774/apple-iphone-felica-tap-to-pay-subway-japan

Apple is planning to add FeliCa technology — the wireless standard used by Japan's mass transit systems to accept payments —  to its next iPhone.

FeliCa is big business — there are 1.9 million FeliCa payment terminals across the country, according to the Bank of Japan, handling around $46 billion of transactions last year. In comparison, Bloomberg notes that there are only 1.3 million NFC terminals in the United States, and 320,000 in the United Kingdom according to data from Let's Talk Payments and the UK Cards Association.

Works on :
* train journey
* vending machines
* convenience stores
* games on Nintendo's Wii U console
* ...

http://www.forbes.com/sites/ilyapozin/2015/09/10/3-trends-in-mobile-payments-you-need-to-know-about/#5125f54c3619

* While these solutions have a lot in common, they aren’t one-to-one copies of each other. Broadly, they rely on near field communication (NFC), tokenization, fingerprint readers and a mix of other technologies to provide a seamless option for users.
* allows for a seamless experience between online and offline, easily allowing customers to make purchases with their phones and redeem those purchases offline.
* There are a host of payment apps that allow users to easily move money to other individuals without the hassle of cash.

https://techcrunch.com/2016/06/17/the-evolution-of-the-mobile-payment/

* It’s anticipated that there will be more than 4.8 billion individuals using a mobile phone by the end of 2016.
* 90 percent of smartphone users will have made a mobile payment. It’s estimated that by 2017, there will be $60 billion in mobile payment sales by 2020.

